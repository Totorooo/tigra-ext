#!/opt/local/bin/perl
#
use warnings;
use strict;
use Getopt::Std;
use File::Basename;
#use breakdancer;
#use vcf;

# Function: This is a pipeline to infer SV. There are two steps: 1. Run TIGRA given breakdancer formatted input. 2. Given a contig (fasta file), align by bwa mem to reference; 2. Call revised Zechen's infer_sv.pl and write the VCF.
# Usage: perl TIGRA-ext.pl [options] <SV_file> <bam|or blank>
# Author: Xian Fan, Zechen Chong, Ken Chen 

my $script_dir = dirname($0);
my $version = '0.0';
my %opts = (d=>'tmp',o=>'./sv.vcf', s=>0, k=>'15,25', a=>500, l=>500, q=>1, p=>10000, h=>500, B=>'bwa', e=>'', r=>'NA', u=>'tigra', S=>10, L=>20, F=>500, I=>'intersectBed', E=>'NA');

my ($SV_file, $bam_file);
my ($ref_file, $contig_file, $sam_file, $prefix, $options);
my ($bwa, $intersectBed);

getopts('d:o:u:r:e:s:k:c:a:l:q:p:mbf:h:T:B:S:L:F:I:E:', \%opts);
die("
		Usage: perl TIGRA-ext.pl [-r reference.fa] [-B ~/pkg/bwa/bwa] [-I ~/pkg/bedtools/bin/intersectBed] [-T ~/pkg/TIGRA/] [other_options] <SV_file> <bam|or blank>
		Options:
		-d	DIR	Directory where all intermediate files are saved [$opts{d}]
		-o	FILE	Output VCF including path [$opts{o}]
		-u  STR Prefix in temporary files, assembled contigs, converted input file and sam [$opts{u}]
		-r  STR Reference file. REQUIRED. 
		-e  STR Options for the alignment tool. By default none.
		-s  NUM    Skip the 1st step (TIGRA) by [>1] or both (TIGRA and alignment) [>2]. By default all are on
		-k	INT	kmers, separated by comma in iteration [$opts{k}] (TIGRA option)
		-c	STR	only assemble calls on chromosome [STR] (TIGRA option)
		-l	INT	Assembly [$opts{l}] bp from the SV breakpoints (TIGRA option)
		-a	INT	Assembly [$opts{a}] bp into the SV breakpoints (TIGRA option)
		-q	INT	Only assemble reads with mapping quality over [$opts{q}] (TIGRA option)
		-p	INT	Ignore cases that have average read depth greater than [$opts{p}] (TIGRA option)
		-m		Add mate for assembly. By default off. (TIGRA option)
		-b		Input is breakdancer format. By default in vcf format. (TIGRA option)
		-f	STR	Bam list, provided when bam is not in argument but presented in VCF/breakdancer.
		-h  INT Skip complex contig graphs with more than [$opts{h}] nodes. (TIGRA option)
		-T  STR TIGRA binary directory if not being able to be called from anywhere.
		-B	STR	Full path BWA binary. [$opts{B}]
		-S	INT     Insertion gap limit [$opts{S}] (BREPA option)
		-L	INT     Minimum length of micro-insertion for checking if it is templated [$opts{L}] (BREPA option)
		-F	INT	Intersection flanking region size [500] (intersectBed)
		-I	STR	Intersectbed binary. REQUIRED if not callable from anywhere [intersectBed]
        -E  STR TIGRA options, other than -k, -c, -l, -a, -q, -p, -m, -b, -h. 
		") unless($#ARGV >= 0 && $opts{r} ne "NA");
		$SV_file = $ARGV[0];
		$bam_file = defined $ARGV[1] ? $ARGV[1] : "NA";

		$contig_file = $ARGV[0] if(defined $opts{s} && $opts{s} == 2);
		$sam_file = $ARGV[0] if(defined $opts{s} && $opts{s} == 3);

		if(!-d $opts{d}){
			`mkdir $opts{d}`;
		}

$prefix = $opts{u};
$bwa = $opts{B};
$options->{BWA} = $opts{e};
$ref_file->{fa} = $opts{r};

# prepare TIGRA alignment options and arguments
# convert other forms to breakdancer

# step 1. Call TIGRA
my $new_SV_file;
if(!defined $opts{s} || defined $opts{s} && $opts{s} <= 1){
	if(! $opts{b}){
		$new_SV_file = "$opts{d}/$prefix.sv";
		&convert_vcf_breakdancer($SV_file, $new_SV_file);
	}
	else{
		$new_SV_file = $SV_file;
	}
	$contig_file = "$opts{d}/$prefix.ctg.fa";

	if(!defined $opts{s} || defined $opts{s} && $opts{s} <= 1){
		$options->{TIGRA} = "-b -p $opts{p} -q $opts{q} -l $opts{l} -a $opts{a} -k $opts{k} -h $opts{h} -o $contig_file -R $ref_file->{fa}";
		$options->{TIGRA} .= " -m" if(defined $opts{m});
		$options->{TIGRA} .= " -f $opts{f}" if(defined $opts{f});
		$options->{TIGRA} .= " -c $opts{c}" if(defined $opts{c});
        $options->{TIGRA} .= " " . $opts{E} if(defined $opts{E} && $opts{E} ne "NA");
		die "Bam is not given." if($bam_file eq "NA"); 
		&call_TIGRA($new_SV_file, $options, $bam_file); 
	}
}

# step 2. Call BWA alignment
if(!defined $opts{s} || defined $opts{s} && $opts{s} <= 2){
	$sam_file = "$opts{d}/$prefix.sam";
	&call_alignment($contig_file, $sam_file, $ref_file, $options);
}

# step 3. infer sv; the difference from BREPA is that the input of this function is the sam file instead of raw contig/reads
&call_infer_sv($sam_file);

# step 4. overlap with putative calls. available only if starting from the putative calls.
if(!defined $opts{s} || defined $opts{s} && $opts{s} <= 1){
	$intersectBed = $opts{I};
	if(-e $intersectBed){
		&overlap($new_SV_file, "$opts{o}.beforeIntersectBed", $opts{d}, $opts{F}, $opts{I});
	}
	else{
		die "intersectBed not given or not existing\n";
	}
}
1;

# convert a vcf file to breakdancer
sub convert_vcf_breakdancer{
	my ($SV_file, $new_SV_file) = @_;
# sample starting column (0-based)
	my $n = 10;
	my $hash = &read_vcf_w_sample($SV_file);
	&write_breakdancer($hash, $new_SV_file);
}

sub call_TIGRA{
	my ($SV_file, $options, $bam) = @_;
	my $tigra_path = "";
	if(defined $opts{T}){
		$tigra_path = $opts{T};
		$tigra_path .= "/" if($opts{T} !~ /\/$/);
	}
	my $command = "$tigra_path" . "tigra-sv $options->{TIGRA} $SV_file"; 
	$command .= " $bam" if($bam ne "NA");
	`$command`;
}

sub call_alignment{
	my ($contig_file, $sam_file, $ref_file, $options) = @_;
# bwa mem
	my $ref = $ref_file->{fa};
	`$bwa mem $options->{BWA} $ref $contig_file > $sam_file`;
}

sub call_infer_sv{
	my $sam_file = shift;
	if(-e $sam_file){
		`perl $script_dir/infer_sv.pl -o $opts{o}.beforeIntersectBed -l $opts{L} -s $opts{S} $sam_file`;
#		print "perl infer_sv.pl < $sam_file > $opts{o}\n";

	}
	else{
		die "Alignment error: no sam file $sam_file exists\n";
	}
}

# overlap
sub overlap{
	my ($putative_bd, $inferred_vcf, $tmp_dir, $flank, $intersectBed) = @_;
	&breakdancer2bed($putative_bd, $tmp_dir, "a.bed", $flank);
	&vcf2bed($inferred_vcf, $tmp_dir, "b.bed", $flank);
	`$intersectBed -wo -a $tmp_dir/a.bed -b $tmp_dir/b.bed > $tmp_dir/intersect.bed`;
	my $hash;
	open intersect_fh, "<$tmp_dir/intersect.bed" or die $!;
	while(<intersect_fh>){
		chomp;
		my @a = split(/\t/, $_);
		$hash->{$a[7]}->{line} = $a[8];
		$hash->{$a[7]}->{overlap}->{$a[3]}->{join(":", @a[0 .. 2])} = 1;
	}
	close intersect_fh;
	open out_fh, ">$opts{o}" or die $!;
	foreach my $index_call (sort keys %$hash){
		foreach my $index_putative (sort keys %{$hash->{$index_call}->{overlap}}){
			if(scalar(keys %{$hash->{$index_call}->{overlap}->{$index_putative}}) == 2){
				print out_fh join("\t", split(/@/, $hash->{$index_call}->{line})) . "\n";
				last;
			}
		}
	}
	close out_fh;

}

# convert breakdancer two bed for putative calls
sub breakdancer2bed{
	my ($putative_bd, $tmp_dir, $file_name, $flank) = @_;
	open out_fh, ">$tmp_dir/$file_name" or die $!;
	open bd_fh, "<$putative_bd" or die $!;
	while(<bd_fh>){
		chomp;
		next if($_ =~ /^#/);
		my @a = split(/\t/, $_);
		my $str = join(":", @a[0 .. 1], @a[3 .. 4]);
		print out_fh join("\n", join("\t", ($a[0], $a[1] - $flank, $a[1] + $flank, $str)), join("\t", ($a[3], $a[4] - $flank, $a[4] + $flank, $str))) . "\n";
	}
	close bd_fh;
	close out_fh;
}

# convert vcf to bed, treating start and end independently, each with a flanking size
sub vcf2bed{
	my ($vcf_file, $tmp_dir, $file_name, $flank) = @_;
	open out_fh, ">$tmp_dir/$file_name" or die $!;
	open vcf_fh, "<$vcf_file" or die $!;
	while(<vcf_fh>){
		chomp;
		next if($_ =~ /^#/);
		my @a = split(/\t/, $_);
		my ($chr1, $pos1, $chr2, $pos2);
		$chr1 = $a[0];
		$pos1 = $a[1];
		if($_ =~ /CHR2=(\S+);END=(\d+)/i){
			$chr2 = $1;
			$pos2 = $2;
			my $str = join(":", $chr1, $pos1, $chr2, $pos2);
			my $line = join("@", split(/\t/, $_));
			print out_fh join("\n", join("\t", ($chr1, $pos1 - $flank, $pos1 + $flank, $str, $line)), join("\t", ($chr2, $pos2 - $flank, $pos2 + $flank, $str, $line))) . "\n";
		}
	}
	close vcf_fh;
	close out_fh;
}

sub write_breakdancer{
# it reads a hash with $str (chr1:start:chr2:end) -> @samples and write to a breakdancer file
	my ($hash, $sv) = @_;
	open SV, ">$sv" or die $!;
	foreach my $key (keys %$hash){
		my @a = split(/:/, $key);
# assuming ITX for all
		print SV join("\t", @a[0 .. 1], "+", @a[2 .. 3], "-", "ITX", $a[3] - $a[1], "99", "10");
		foreach my $s (@{$hash->{$key}->{samples}}){
			print SV "\t" . join("|", $s, 5);
		}
		print SV "\n";
	}
	close SV;
}



sub read_vcf_w_sample{
# It reads a vcf formatted file and record it to a hash in a form of ID -> chr1, pos1, chr2, pos2, @samples with variants
	my ($f) = @_;
	my $hash;
	my @samples;
	open fh_, "<$f" or die $!;
	while(<fh_>){
		next if($_ =~ /^##/);
		chomp;        
		my @a = split(/\s+/, $_);
		next if(scalar(@a) == 0);
		if($_ =~ /^#/){
			@samples = @a[9 .. $#a];
		}       
		else{
# record variant samples
			my $key = $a[2];
			my ($chr1, $start) = @a[0 .. 1];
			my ($chr2, $end);
			if($_ =~ /end=(\d+)/i){
				$end = $1;
				if($_ =~ /chr2=(\w+)/i){
					$chr2 = $1;
				}
				else{
# assume the same if no chr2
					$chr2 = $chr1;
				}
			}
            $key = join(":", ($chr1, $start, $chr2, $end));
# deal with samples
			my @sample;
			my $tag = 0;
			foreach (@a[9 .. $#a]){
				if($_ =~ /^0\/1/ || $_ =~ /^1\/1/ || $_ =~ /^0\|1/ || $_ =~ /^1\|1/){
					push @sample, $samples[$tag];
				}
				$tag ++;
			}
            $hash->{$key}->{line} = $_;
			$hash->{$key}->{samples} = \@sample;
		}

	}
	close fh_;

	return $hash;
}


